import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { AuthenticateService } from '../services/authenticate.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ItemsService } from '../services/items.service';
import {NgbModal, ModalDismissReasons, NgbModalOptions} from '@ng-bootstrap/ng-bootstrap';


declare var $: any;

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit, OnDestroy {
  showMenu = false;
  page = 0;
  data = [];
  mySubscription: any;
  search = '';
  showCategory = false;
  sortOptions = false;

  constructor(private auth: AuthenticateService,
              private spinner: NgxSpinnerService,
              private shop: ItemsService,
              private route: Router,
              private actRoute: ActivatedRoute,
              private modalService: NgbModal
              ) {
                this.actRoute.queryParams.subscribe(params => {
                  if (params.search) {
                    this.search = params.search;
                  }
              });
              }
  ngOnInit() {
    $('#waterfall').NewWaterfall({
      width: 200,
      delay: 60,
      repeatShow: false
  });
    this.auth.currentMessage.subscribe(message => this.showMenu = message);
    this.getProducts();
  }
  ngOnDestroy() {
    if (this.mySubscription) {
      this.mySubscription.unsubscribe();
    }
  }

getProducts() {
    this.spinner.show();
    this.shop.searchItems(this.page, this.search).subscribe((res: any) => this.onSuccess(res));
  }
onSuccess(res) {
    console.log(res);
    if (res !== undefined) {
      this.spinner.hide();
      if (res.message === undefined) {
        res.products.forEach((item: any) => {
          this.data.push(item);
        });
      }
    }
  }
onScroll() {
    this.page = this.page + 12;
    this.getProducts();
  }
}

