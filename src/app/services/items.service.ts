import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};
@Injectable({
  providedIn: 'root'
})
export class ItemsService {

  constructor(private http: HttpClient) { }

  getItems(page, categoryId, publishVal): Observable <any> {
    let qparam = '';
    if (categoryId !== undefined && categoryId !== '') {
      qparam += '&idCategory=' + categoryId;
    }
    if (publishVal !== undefined && publishVal !== '') {
      if (publishVal === 'true') {
        qparam += '&isProcured=' + 1;
      } else {
        qparam += '&isProcured=' + 0;
      }
    }
    return this.http.get<any>(environment.endPoint + 'scrape/products/get?start=' + page + '&count=12' + qparam);
  }
  searchItems(page, searchString): Observable <any> {
    // tslint:disable-next-line: max-line-length
    return this.http.get<any>(environment.endPoint + 'solr/getSearch?searchString=' + searchString + '&searchType=products&start=' + page + '&count=12');
  }
  getBoardItems() {
    return this.http.get<any>(environment.endPoint + 'scrape/products/get?start=0&count=60');
  }
  scrape(url): Observable <any> {
    return this.http.get<any>(environment.endPoint + 'scrape?url=' + url);
  }
  getItem(id): Observable <any> {
    return this.http.get<any>(environment.endPoint + 'scrape/productDetails/' + id);
  }
  getCounts(): Observable <any> {
    return this.http.get<any>(environment.endPoint + 'dashboard');
  }
  procure(prod): Observable <any> {
    return this.http.post<any>(environment.endPoint + 'product/add/', prod);
  }
  addImage(prod): Observable <any> {
    return this.http.post<any>(environment.endPoint + 'product/image/add/', prod);
  }
  getCategories(): Observable <any> {
    return this.http.get<any>(environment.endPoint + 'categories');
  }
  unpublish(id): Observable <any> {
    return this.http.get<any>(environment.endPoint + 'product/unPublish?productId=' + id);
  }
  publish(id): Observable <any> {
    return this.http.get<any>(environment.endPoint + 'product/publish?productId=' + id);
  }
  publishAdd(id): Observable <any> {
    return this.http.get<any>(environment.endPoint + 'product/add?productId=' + id);
  }
}
