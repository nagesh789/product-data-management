import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ItemsComponent } from './items/items.component';
import { ItemDetailsComponent } from './item-details/item-details.component';
import { BookmarkComponent } from './bookmark/bookmark.component';
import { BookmarkletComponent } from './bookmarklet/bookmarklet.component';
import { AuthGuard } from './services/auth.guard';
import { MoodboardComponent } from './moodboard/moodboard.component';
import { MymoodboardsComponent } from './mymoodboards/mymoodboards.component';
import { SearchComponent } from './search/search.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
    canActivate: [AuthGuard]
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'moodboards',
    component: MoodboardComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'mymoodboards',
    component: MymoodboardsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'items',
    component: ItemsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'search',
    component: SearchComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'item/:id',
    component: ItemDetailsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'bookmarklet',
    component: BookmarkletComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'scrape',
    component: BookmarkComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    scrollPositionRestoration: 'enabled',
    anchorScrolling: 'enabled',
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
