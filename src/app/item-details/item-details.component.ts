import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ItemsService } from '../services/items.service';
import { AuthenticateService } from '../services/authenticate.service';
import { ScrollToTopService } from '../services/scroll-to-top.service';


@Component({
  selector: 'app-item-details',
  templateUrl: './item-details.component.html',
  styleUrls: ['./item-details.component.css']
})
export class ItemDetailsComponent implements OnInit {
  url: string;
  success = false;
  error: string;
  itemInfo: any;
  editTitle = false;
  showMenu = false;
  inhabitrPrice = 0;
  inhabitrPercentage = 20;
  edit_percentage = false;
  constructor(private route: ActivatedRoute,
              private spinner: NgxSpinnerService,
              private item: ItemsService,
              private auth: AuthenticateService,
              private stp: ScrollToTopService) { }

  ngOnInit() {
      this.spinner.show();
      this.auth.currentMessage.subscribe(message => this.showMenu = message);
      this.route.paramMap.subscribe((params) => {
          this.getItem(params.get('id'));
      });
      this.stp.setScrollTop();
  }
  showEditTitle() {
    this.editTitle = !this.editTitle;
  }
  getPercentageValue(price, percentage) {
    const result = (parseFloat(price) * parseFloat(percentage)) / 100;
    return result;
  }
  getItem(id) {
    this.spinner.hide();
    this.item.getItem(id).subscribe(
      resp => {
        this.spinner.hide();
        this.itemInfo = resp;
        // tslint:disable-next-line: max-line-length
        this.inhabitrPrice = parseFloat(this.itemInfo.salePrice) + this.getPercentageValue(this.itemInfo.salePrice, this.inhabitrPercentage);
        this.itemInfo.categories.forEach(element => {
          delete element.createdTime;
        });
      }, err => {
        this.spinner.hide();
      });
  }
  procure() {
    const itemObj =  this.itemInfo;
    delete itemObj.createdTime;
    delete itemObj.updatedDate;
    this.spinner.show();
    this.success = false;
    this.item.procure(itemObj).subscribe(
      resp => {
        this.spinner.hide();
        this.addImage(resp);
      }, err => {
        this.spinner.hide();
      }
    );
  }
  unpublish() {
    this.spinner.show();
    this.item.unpublish(this.itemInfo.id).subscribe(
      resp => {
        this.spinner.hide();
        this.itemInfo.isProcured = 0;
      },
      err => {
        this.spinner.hide();
      }
    );
  }
  publish() {
    this.spinner.show();
    this.item.publish(this.itemInfo.id).subscribe(
      resp => {
        this.spinner.hide();
        this.publishAdd();
      },
      err => {
        this.spinner.hide();
      }
    );
  }
  publishAdd() {
    this.spinner.show();
    this.item.publishAdd(this.itemInfo.id).subscribe(
      resp => {
        this.spinner.hide();
        this.addImage(resp);
      },
      err => {
        this.spinner.hide();
      }
    );
  }
  addImage(obj) {
    this.spinner.show();
    this.item.addImage(obj).subscribe(
      resp => {
        this.spinner.hide();
        this.itemInfo.isProcured = 1;
        this.itemInfo.updatedDate = new Date();
        this.success = true;
      }, err => {
        this.spinner.hide();
      }
    );
  }
  manage_percentage() {
    this.edit_percentage = !this.edit_percentage;
  }

}
