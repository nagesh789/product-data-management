import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgxSpinnerModule } from 'ngx-spinner';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { InfiniteScrollModule} from 'ngx-infinite-scroll';
import { ColorPickerModule } from 'ngx-color-picker';

import { HeaderComponent } from './header/header.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ItemsComponent } from './items/items.component';
import { ItemDetailsComponent } from './item-details/item-details.component';
import { BookmarkletComponent } from './bookmarklet/bookmarklet.component';
import { BookmarkComponent } from './bookmark/bookmark.component';
import { OrderByPipe } from './pipes/order-by.pipe';
import { CapitalizePipe } from './pipes/capitalize.pipe';
import { MoodboardComponent } from './moodboard/moodboard.component';
import { MymoodboardsComponent } from './mymoodboards/mymoodboards.component';
import { SafePipe } from './safe.pipe';
import { SearchComponent } from './search/search.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LoginComponent,
    DashboardComponent,
    ItemsComponent,
    ItemDetailsComponent,
    BookmarkletComponent,
    BookmarkComponent,
    OrderByPipe,
    CapitalizePipe,
    MoodboardComponent,
    MymoodboardsComponent,
    SafePipe,
    SearchComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    NgbModule,
    AngularFontAwesomeModule,
    InfiniteScrollModule,
    ColorPickerModule,
    NgxSpinnerModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
