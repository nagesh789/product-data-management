import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { AuthenticateService } from '../services/authenticate.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ItemsService } from '../services/items.service';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit, OnDestroy {
  showMenu = false;
  dashboardCounts: any = {};
  mySubscription: any;
  constructor(private auth: AuthenticateService,
              private spinner: NgxSpinnerService,
              private shop: ItemsService,
              private route: Router) {
                // tslint:disable-next-line: only-arrow-functions
                this.route.routeReuseStrategy.shouldReuseRoute = function() {
                  return false;
                };
                this.mySubscription = this.route.events.subscribe((event) => {
                  if (event instanceof NavigationEnd) {
                    // Trick the Route into believing it's last link wasn't previously loaded
                    this.route.navigated = false;
                  }
                }); }
  ngOnInit() {
    this.auth.currentMessage.subscribe(message => this.showMenu = message);
    this.getCounts();
  }
  ngOnDestroy() {
    if (this.mySubscription) {
      this.mySubscription.unsubscribe();
    }
  }
  getCounts() {
    this.spinner.show();
    this.shop.getCounts().subscribe(
      resp => {
        this.spinner.hide();
        this.dashboardCounts = resp;
      },
      err => {
        this.spinner.hide();
      }
    );
  }
}
