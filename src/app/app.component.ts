import { Component } from '@angular/core';
import { Location } from '@angular/common';
import { NavigationEnd, Router, Event} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'product-management';
  showHeader: any = false;

  constructor(private rte: Router, private location: Location) {
    rte.events.subscribe(val => {
      if (location.path().search('login') !== -1 || location.path().search('scrape') !== -1) {
        this.showHeader = false;
      } else {
        this.showHeader = true;
      }
    });

  }
}
