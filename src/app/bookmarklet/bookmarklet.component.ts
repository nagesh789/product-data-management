
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { AuthenticateService } from '../services/authenticate.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ItemsService } from '../services/items.service';

@Component({
  selector: 'app-bookmarklet',
  templateUrl: './bookmarklet.component.html',
  styleUrls: ['./bookmarklet.component.css']
})
export class BookmarkletComponent implements OnInit, OnDestroy {

  showMenu = false;
  mySubscription: any;
  constructor(private auth: AuthenticateService,
              private spinner: NgxSpinnerService,
              private shop: ItemsService,
              private route: Router) {
                // tslint:disable-next-line: only-arrow-functions
                this.route.routeReuseStrategy.shouldReuseRoute = function() {
                  return false;
                };
                this.mySubscription = this.route.events.subscribe((event) => {
                  if (event instanceof NavigationEnd) {
                    // Trick the Route into believing it's last link wasn't previously loaded
                    this.route.navigated = false;
                  }
                }); }
  ngOnInit() {
    this.auth.currentMessage.subscribe(message => this.showMenu = message);
  }
  ngOnDestroy() {
    if (this.mySubscription) {
      this.mySubscription.unsubscribe();
    }
  }
}
