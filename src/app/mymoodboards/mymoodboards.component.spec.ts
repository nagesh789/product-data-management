import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MymoodboardsComponent } from './mymoodboards.component';

describe('MymoodboardsComponent', () => {
  let component: MymoodboardsComponent;
  let fixture: ComponentFixture<MymoodboardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MymoodboardsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MymoodboardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
