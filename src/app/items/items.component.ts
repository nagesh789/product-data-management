import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { AuthenticateService } from '../services/authenticate.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ItemsService } from '../services/items.service';
import {NgbModal, ModalDismissReasons, NgbModalOptions} from '@ng-bootstrap/ng-bootstrap';


declare var $: any;

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit, OnDestroy {
  showMenu = false;
  page = 0;
  data = [];
  mySubscription: any;
  modalOptions: NgbModalOptions;
  closeResult: string;
  categories: any;
  selectedCategory: any = {};
  selectedCategoryId: any = '';
  publishVal = '';
  showCategory = false;
  sortOptions = false;

  constructor(private auth: AuthenticateService,
              private spinner: NgxSpinnerService,
              private shop: ItemsService,
              private route: Router,
              private actRoute: ActivatedRoute,
              private modalService: NgbModal
              ) {
                this.modalOptions = {
                  size: 'lg',
                  backdrop: 'static',
                  backdropClass: 'customBackdrop',
                  centered: true
                };
                this.actRoute.queryParams.subscribe(params => {
                  if (params.publish) {
                    this.publishVal = params.publish;
                  }
                  if (params.categoryname) {
                    this.selectedCategory.name = params.categoryname;
                    this.selectedCategory.id = params.categoryid;
                    this.selectedCategoryId = params.categoryid;
                  }
              });
              }
  ngOnInit() {
    $('#waterfall').NewWaterfall({
      width: 200,
      delay: 60,
      repeatShow: false
  });
    this.auth.currentMessage.subscribe(message => this.showMenu = message);
    this.getProducts();
    this.getCategories();
  }
  sortBy(val) {
    if (this.selectedCategoryId) {
      // tslint:disable-next-line: max-line-length
      this.route.navigate(['/items'], { queryParams: { publish: val, categoryname: this.selectedCategory.name , categoryid: this.selectedCategory.id }});
    } else {
      this.route.navigate(['/items'], { queryParams: { publish: val } });
    }
  }
  ngOnDestroy() {
    if (this.mySubscription) {
      this.mySubscription.unsubscribe();
    }
  }
  open(content, type) {
    this.showCategory = false;
    this.sortOptions = false;
    if (type === 'sort') {
      this.sortOptions = true;
    } else if (type === 'category') {
      this.showCategory = true;
    }
    this.modalService.open(content, this.modalOptions).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
  clearAll(type) {
    if (type === 'sort') {
      if (this.selectedCategoryId) {
        // tslint:disable-next-line: max-line-length
        this.route.navigate(['/items'], { queryParams: { categoryname: this.selectedCategory.name , categoryid: this.selectedCategory.id }});
      } else {
        this.route.navigate(['/items']);
      }
    } else if (type === 'category') {
      if (this.publishVal === 'true' || this.publishVal === 'false') {
        this.route.navigate(['/items'], { queryParams: { publish: this.publishVal }});
      } else {
        this.route.navigate(['/items']);
      }
    } else {
      this.route.navigate(['/items']);
    }
  }
  getCategories() {
    this.shop.getCategories().subscribe(
      resp => {
        this.categories = resp;
      }, err => {
      }
    );
  }
  filterItems(val) {
    this.page = 0;
    this.data = [];
    if (val === 'All') {
      this.selectedCategory = {};
      this.selectedCategoryId = '';
      this.getProducts();
    } else {
      this.selectedCategory = val;
      this.selectedCategoryId = val.id;
      if (this.publishVal === 'true' || this.publishVal === 'false') {
        this.route.navigate(['/items'], { queryParams: { publish: this.publishVal, categoryname: val.name , categoryid: val.id }});
      } else {
        this.route.navigate(['/items'], { queryParams: { categoryname: val.name , categoryid: val.id }});
      }
      this.getProducts();
    }
  }
getProducts() {
    this.spinner.show();
    this.shop.getItems(this.page, this.selectedCategoryId, this.publishVal).subscribe((res: any) => this.onSuccess(res));
  }
onSuccess(res) {
    console.log(res);
    if (res !== undefined) {
      this.spinner.hide();
      if (res.message === undefined) {
        res.forEach((item: any) => {
          this.data.push(item);
        });
      }
    }
  }
onScroll() {
    this.page = this.page + 12;
    this.getProducts();
  }
}
