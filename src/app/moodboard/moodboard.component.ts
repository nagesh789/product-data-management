import { Component, OnInit, AfterViewInit } from '@angular/core';
import { AuthenticateService } from '../services/authenticate.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ItemsService } from '../services/items.service';

import 'fabric';
import * as jsPDF from 'jspdf';
import html2canvas from 'html2canvas';
import { Router } from '@angular/router';
import { AnonymousSubject } from 'rxjs/internal/Subject';
declare const fabric: any;


@Component({
  selector: 'app-root',
  templateUrl: './moodboard.component.html',
  styleUrls: ['./moodboard.component.css']
})

export class MoodboardComponent implements OnInit {

  constructor(private auth: AuthenticateService,
              private spinner: NgxSpinnerService,
              private shop: ItemsService,
              private route: Router) { }
  showMenu = false;
  showStudio = false;
  showBoards = true;
  listQuote = false;
  hideOnPrint = false;
  staticImages = [
    {name: 'ProjectComet', img: 'assets/img/p1.jpg'},
    {name: 'ProjectLiving', img: 'assets/img/p2.jpg'},
    {name: 'ProjectOrange', img: 'assets/img/p3.jpg'},
    {name: 'ProjectBlueSea', img: 'assets/img/p4.jpeg'},
    {name: 'ProjectBoho', img: 'assets/img/p5.jpeg'},
    {name: 'ProjectOffice', img: 'assets/img/p6.jpeg'}
  ];
  newName = '';
  private canvas: any;
  data = [];
  private props: any = {
    canvasFill: '#ffffff',
    canvasImage: '',
    id: null,
    opacity: null,
    fill: null,
    fontSize: null,
    lineHeight: null,
    charSpacing: null,
    fontWeight: null,
    fontStyle: null,
    textAlign: null,
    fontFamily: null,
    TextDecoration: ''
  };

  private textString: string;
  url: String = '';
  size: any = {
    width: 600,
    height: 300
  };

  private json: any;
  private globalEditor: Boolean = false;
  private textEditor: Boolean = false;
  private imageEditor: Boolean = false;
  private figureEditor: Boolean = false;
  selected: any;
  getQuote() {
    this.listQuote = !this.listQuote;
  }

  ngOnInit() {
    this.auth.currentMessage.subscribe(message => this.showMenu = message);
    // setup front side canvas
    // this.canvas = new fabric.Canvas('canvas', {
    //   hoverCursor: 'pointer',
    //   selection: true,
    //   selectionBorderColor: 'blue'
    // });
    // this.getProducts();

    // this.canvas.on({
    //   'object:moving': (e) => { },
    //   'object:modified': (e) => { },
    //   'object:selected': (e) => {

    //     const selectedObject = e.target;
    //     this.selected = selectedObject;
    //     selectedObject.hasRotatingPoint = true;
    //     selectedObject.transparentCorners = false;

    //     this.resetPanels();

    //     if (selectedObject.type !== 'group' && selectedObject) {

    //       this.getId();
    //       this.getOpacity();

    //       switch (selectedObject.type) {
    //         case 'rect':
    //         case 'circle':
    //         case 'triangle':
    //           this.figureEditor = true;
    //           this.getFill();
    //           break;
    //         case 'i-text':
    //           this.textEditor = true;
    //           this.getLineHeight();
    //           this.getCharSpacing();
    //           this.getBold();
    //           this.getFontStyle();
    //           this.getFill();
    //           this.getTextDecoration();
    //           this.getTextAlign();
    //           this.getFontFamily();
    //           break;
    //         case 'image':
    //           console.log('image');
    //           break;
    //       }
    //     }
    //   },
    //   'selection:cleared': (e) => {
    //     this.selected = null;
    //     this.resetPanels();
    //   }
    // });

    // this.canvas.setWidth(this.size.width);
    // this.canvas.setHeight(this.size.height);

    // this.setCanvasFill();

  }
  createNewBoard() {
    // setup front side canvas
      if (this.newName !== '') {
      this.showBoards = false;
      this.showStudio = true;
      this.spinner.show();
      setTimeout(() => {
      this.canvas = new fabric.Canvas('canvas', {
        hoverCursor: 'pointer',
        selection: true,
        selectionBorderColor: 'blue'
      });

      this.canvas.on({
        'object:moving': (e) => { },
        'object:modified': (e) => { },
        'object:selected': (e) => {

          const selectedObject = e.target;
          this.selected = selectedObject;
          selectedObject.hasRotatingPoint = true;
          selectedObject.transparentCorners = false;

          this.resetPanels();

          if (selectedObject.type !== 'group' && selectedObject) {

            this.getId();
            this.getOpacity();

            switch (selectedObject.type) {
              case 'rect':
              case 'circle':
              case 'triangle':
                this.figureEditor = true;
                this.getFill();
                break;
              case 'i-text':
                this.textEditor = true;
                this.getLineHeight();
                this.getCharSpacing();
                this.getBold();
                this.getFontStyle();
                this.getFill();
                this.getTextDecoration();
                this.getTextAlign();
                this.getFontFamily();
                break;
              case 'image':
                console.log('image');
                break;
            }
          }
        },
        'selection:cleared': (e) => {
          this.selected = null;
          this.resetPanels();
        }
      });

      this.canvas.setWidth(this.size.width);
      this.canvas.setHeight(this.size.height);

      this.setCanvasFill();
      this.spinner.hide();
    }, 3000);
      } else {
        alert('Please Enter Title');
      }

  }
  listBoards() {
    this.newName = '';
    this.showBoards = true;
    this.showStudio = false;
  }
  captureScreen() {
    this.hideOnPrint = true;
    this.listQuote = true;
    this.spinner.show();
    setTimeout(() => {
    const data = document.getElementById('contentToConvert');
    html2canvas(data).then(canvas => {
      // Few necessary setting options
      const imgWidth = 208;
      const pageHeight = 295;
      const imgHeight = canvas.height * imgWidth / canvas.width;
      const heightLeft = imgHeight;
      const contentDataURL = canvas.toDataURL('image/png');
      const pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF
      const position = 0;
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
      pdf.save(this.newName + '.pdf'); // Generated PDF
      this.hideOnPrint = false;
      this.spinner.hide();

    });
  }, 3000);
  }
  openBoard(obj) {
    // setup front side canvas
      this.showBoards = false;
      this.showStudio = true;
      this.newName = obj.name;
      this.spinner.show();
      setTimeout(() => {
      this.canvas = new fabric.Canvas('canvas', {
        hoverCursor: 'pointer',
        selection: true,
        selectionBorderColor: 'blue'
      });

      this.canvas.on({
        'object:moving': (e) => { },
        'object:modified': (e) => { },
        'object:selected': (e) => {

          const selectedObject = e.target;
          this.selected = selectedObject;
          selectedObject.hasRotatingPoint = true;
          selectedObject.transparentCorners = false;

          this.resetPanels();

          if (selectedObject.type !== 'group' && selectedObject) {

            this.getId();
            this.getOpacity();

            switch (selectedObject.type) {
              case 'rect':
              case 'circle':
              case 'triangle':
                this.figureEditor = true;
                this.getFill();
                break;
              case 'i-text':
                this.textEditor = true;
                this.getLineHeight();
                this.getCharSpacing();
                this.getBold();
                this.getFontStyle();
                this.getFill();
                this.getTextDecoration();
                this.getTextAlign();
                this.getFontFamily();
                break;
              case 'image':
                console.log('image');
                break;
            }
          }
        },
        'selection:cleared': (e) => {
          this.selected = null;
          this.resetPanels();
        }
      });

      this.canvas.setWidth(this.size.width);
      this.canvas.setHeight(this.size.height);

      this.setCanvasFill();
      this.addImageOnCanvas(obj.img);
      this.spinner.hide();
    }, 3000);


  }


  /*------------------------Block elements------------------------*/

  // Block "Size"

  changeSize(event: any) {
    this.canvas.setWidth(this.size.width);
    this.canvas.setHeight(this.size.height);
  }

  // Block "Add text"

  addText() {
    const textString = this.textString;
    const text = new fabric.IText(textString, {
      left: 10,
      top: 10,
      fontFamily: 'helvetica',
      angle: 0,
      fill: '#000000',
      scaleX: 0.5,
      scaleY: 0.5,
      fontWeight: '',
      hasRotatingPoint: true
    });
    this.extend(text, this.randomId());
    this.canvas.add(text);
    this.selectItemAfterAdded(text);
    this.textString = '';
  }

  // Block "Add images"

  getImgPolaroid(event: any) {
    const el = event.target;
    fabric.Image.fromURL(el.src, (image) => {
      image.set({
        left: 10,
        top: 10,
        angle: 0,
        padding: 10,
        cornersize: 10,
        hasRotatingPoint: true,
        peloas: 12
      });
      image.crossOrigin = 'Anonymous';
      image.setWidth(150);
      image.setHeight(150);
      this.extend(image, this.randomId());
      this.canvas.add(image);
      this.selectItemAfterAdded(image);
    });
  }

  // Block "Upload Image"

  addImageOnCanvas(url) {
    if (url) {
      fabric.Image.fromURL(url, (image) => {
        image.set({
          left: 10,
          top: 10,
          angle: 0,
          padding: 10,
          cornersize: 10,
          hasRotatingPoint: true
        });
        image.crossOrigin = 'anonymous';
        image.setWidth(200);
        image.setHeight(200);
        this.extend(image, this.randomId());
        this.canvas.add(image);
        this.selectItemAfterAdded(image);
      });
    }
  }

  readUrl(event) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      // tslint:disable-next-line: no-shadowed-variable
      reader.onload = (event) => {
        // tslint:disable-next-line: no-string-literal
        this.url = event.target['result'];
      };
      reader.readAsDataURL(event.target.files[0]);
    }
  }

  removeWhite(url) {
    this.url = '';
  }

  // Block "Add figure"

  addFigure(figure) {
    let add: any;
    switch (figure) {
      case 'rectangle':
        add = new fabric.Rect({
          width: 200, height: 100, left: 10, top: 10, angle: 0,
          fill: '#3f51b5'
        });
        break;
      case 'square':
        add = new fabric.Rect({
          width: 100, height: 100, left: 10, top: 10, angle: 0,
          fill: '#4caf50'
        });
        break;
      case 'triangle':
        add = new fabric.Triangle({
          width: 100, height: 100, left: 10, top: 10, fill: '#2196f3'
        });
        break;
      case 'circle':
        add = new fabric.Circle({
          radius: 50, left: 10, top: 10, fill: '#ff5722'
        });
        break;
    }
    this.extend(add, this.randomId());
    this.canvas.add(add);
    this.selectItemAfterAdded(add);
  }

  /*Canvas*/

  cleanSelect() {
    this.canvas.deactivateAllWithDispatch().renderAll();
  }

  selectItemAfterAdded(obj) {
    this.canvas.deactivateAllWithDispatch().renderAll();
    this.canvas.setActiveObject(obj);
  }

  setCanvasFill() {
    if (!this.props.canvasImage) {
      this.canvas.backgroundColor = this.props.canvasFill;
      this.canvas.renderAll();
    }
  }

  extend(obj, id) {
    obj.toObject = (function(toObject) {
      return function() {
        return fabric.util.object.extend(toObject.call(this), {
          id
        });
      };
    })(obj.toObject);
  }

  setCanvasImage() {
    const self = this;
    if (this.props.canvasImage) {
      this.canvas.setBackgroundColor({ source: this.props.canvasImage, repeat: 'repeat' }, function() {
        // self.props.canvasFill = '';
        self.canvas.renderAll();
      });
    }
  }

  randomId() {
    return Math.floor(Math.random() * 999999) + 1;
  }

  /*------------------------Global actions for element------------------------*/

  getActiveStyle(styleName, object) {
    object = object || this.canvas.getActiveObject();
    if (!object) { return ''; }

    return (object.getSelectionStyles && object.isEditing)
      ? (object.getSelectionStyles()[styleName] || '')
      : (object[styleName] || '');
  }


  setActiveStyle(styleName, value, object) {
    object = object || this.canvas.getActiveObject();
    if (!object) { return; }

    if (object.setSelectionStyles && object.isEditing) {
      const style = {};
      style[styleName] = value;
      object.setSelectionStyles(style);
      object.setCoords();
    } else {
      object.set(styleName, value);
    }

    object.setCoords();
    this.canvas.renderAll();
  }


  getActiveProp(name) {
    const object = this.canvas.getActiveObject();
    if (!object) { return ''; }

    return object[name] || '';
  }

  setActiveProp(name, value) {
    const object = this.canvas.getActiveObject();
    if (!object) { return; }
    object.set(name, value).setCoords();
    this.canvas.renderAll();
  }

  clone() {
    const activeObject = this.canvas.getActiveObject(),
      activeGroup = this.canvas.getActiveGroup();

    if (activeObject) {
      let clone;
      switch (activeObject.type) {
        case 'rect':
          clone = new fabric.Rect(activeObject.toObject());
          break;
        case 'circle':
          clone = new fabric.Circle(activeObject.toObject());
          break;
        case 'triangle':
          clone = new fabric.Triangle(activeObject.toObject());
          break;
        case 'i-text':
          clone = new fabric.IText('', activeObject.toObject());
          break;
        case 'image':
          clone = fabric.util.object.clone(activeObject);
          break;
      }
      if (clone) {
        clone.set({ left: 10, top: 10 });
        this.canvas.add(clone);
        this.selectItemAfterAdded(clone);
      }
    }
  }

  getId() {
    this.props.id = this.canvas.getActiveObject().toObject().id;
  }

  setId() {
    const val = this.props.id;
    const complete = this.canvas.getActiveObject().toObject();
    console.log(complete);
    this.canvas.getActiveObject().toObject = () => {
      complete.id = val;
      return complete;
    };
  }

  getOpacity() {
    this.props.opacity = this.getActiveStyle('opacity', null) * 100;
  }

  setOpacity() {
    this.setActiveStyle('opacity', parseInt(this.props.opacity) / 100, null);
  }

  getFill() {
    this.props.fill = this.getActiveStyle('fill', null);
  }

  setFill() {
    this.setActiveStyle('fill', this.props.fill, null);
  }

  getLineHeight() {
    this.props.lineHeight = this.getActiveStyle('lineHeight', null);
  }

  setLineHeight() {
    this.setActiveStyle('lineHeight', parseFloat(this.props.lineHeight), null);
  }

  getCharSpacing() {
    this.props.charSpacing = this.getActiveStyle('charSpacing', null);
  }

  setCharSpacing() {
    this.setActiveStyle('charSpacing', this.props.charSpacing, null);
  }

  getFontSize() {
    this.props.fontSize = this.getActiveStyle('fontSize', null);
  }

  setFontSize() {
    this.setActiveStyle('fontSize', parseInt(this.props.fontSize), null);
  }

  getBold() {
    this.props.fontWeight = this.getActiveStyle('fontWeight', null);
  }

  setBold() {
    this.props.fontWeight = !this.props.fontWeight;
    this.setActiveStyle('fontWeight', this.props.fontWeight ? 'bold' : '', null);
  }

  getFontStyle() {
    this.props.fontStyle = this.getActiveStyle('fontStyle', null);
  }

  setFontStyle() {
    this.props.fontStyle = !this.props.fontStyle;
    this.setActiveStyle('fontStyle', this.props.fontStyle ? 'italic' : '', null);
  }


  getTextDecoration() {
    this.props.TextDecoration = this.getActiveStyle('textDecoration', null);
  }

  setTextDecoration(value) {
    let iclass = this.props.TextDecoration;
    if (iclass.includes(value)) {
      iclass = iclass.replace(RegExp(value, 'g'), '');
    } else {
      iclass += ` ${value}`;
    }
    this.props.TextDecoration = iclass;
    this.setActiveStyle('textDecoration', this.props.TextDecoration, null);
  }

  hasTextDecoration(value) {
    return this.props.TextDecoration.includes(value);
  }


  getTextAlign() {
    this.props.textAlign = this.getActiveProp('textAlign');
  }

  setTextAlign(value) {
    this.props.textAlign = value;
    this.setActiveProp('textAlign', this.props.textAlign);
  }

  getFontFamily() {
    this.props.fontFamily = this.getActiveProp('fontFamily');
  }

  setFontFamily() {
    this.setActiveProp('fontFamily', this.props.fontFamily);
  }

  /*System*/


  removeSelected() {
    const activeObject = this.canvas.getActiveObject(),
      activeGroup = this.canvas.getActiveGroup();

    if (activeObject) {
      this.canvas.remove(activeObject);
      // this.textString = '';
    } else if (activeGroup) {
      const objectsInGroup = activeGroup.getObjects();
      this.canvas.discardActiveGroup();
      const self = this;
      objectsInGroup.forEach(function(object) {
        self.canvas.remove(object);
      });
    }
  }

  bringToFront() {
    const activeObject = this.canvas.getActiveObject();
    const activeGroup = this.canvas.getActiveGroup();
    console.log(activeObject);
    if (activeObject) {
      activeObject.bringToFront();
    } else if (activeGroup) {
      const objectsInGroup = activeGroup.getObjects();
      this.canvas.discardActiveGroup();
      objectsInGroup.forEach((object) => {
        object.bringToFront();
      });
    }
  }

  sendToBack() {
    const activeObject = this.canvas.getActiveObject();
    const activeGroup = this.canvas.getActiveGroup();
    console.log(activeObject);

    if (activeObject) {
      activeObject.sendToBack();
      // activeObject.opacity = 1;
    } else if (activeGroup) {
      const objectsInGroup = activeGroup.getObjects();
      this.canvas.discardActiveGroup();
      objectsInGroup.forEach((object) => {
        object.sendToBack();
      });
    }
  }

  confirmClear() {
    if (confirm('Are you sure?')) {
      this.canvas.clear();
    }
  }

  rasterize() {
    // if (!fabric.Canvas.supports('toDataURL')) {
    //   alert('This browser doesn\'t provide means to serialize canvas to an image');
    // } else {
    //   console.log(this.canvas.toDataURL('image/png'));
    //   // window.open(this.canvas.toDataURL('image/png'));
    //   let image = new Image();
    //   image.src = this.canvas.toDataURL('image/png');
    //   let w = window.open('');
    //   w.document.write(image.outerHTML);
    // }
    console.log('data:image/svg+xml;utf8,' + encodeURIComponent(this.canvas.toSVG()));
    let imgData = new Image();
    imgData.crossOrigin = 'anonymous';
    imgData = this.canvas.toDataURL('image/jpeg', 1.0);
    const pdf = new jsPDF();
    pdf.addImage(imgData, 'JPEG', 0, 0);
    pdf.save(this.newName + '.pdf');

    // window.open(
    //    'data:image/svg+xml;utf8,' +
    //  encodeURIComponent(this.canvas.toSVG()));
      // let w = window.open('');
      // w.document.write(this.canvas.toSVG());
  }

  rasterizeSVG() {
    console.log(this.canvas.toSVG());
    // window.open(
    //   'data:image/svg+xml;utf8,' +
    //   encodeURIComponent(this.canvas.toSVG()));
    // console.log(this.canvas.toSVG())
    // var image = new Image();
    // image.src = this.canvas.toSVG()
    const w = window.open('');
    w.document.write(this.canvas.toSVG());
  }


  saveCanvasToJSON() {
    const json = JSON.stringify(this.canvas);
    localStorage.setItem('Kanvas', json);
    console.log('json');
    console.log(json);

  }

  loadCanvasFromJSON() {
    const CANVAS = localStorage.getItem('Kanvas');
    console.log('CANVAS');

    // and load everything from the same json
    this.canvas.loadFromJSON(CANVAS, () => {
      console.log('CANVAS untar');
      console.log(CANVAS);

      // making sure to render canvas at the end
      this.canvas.renderAll();

      // and checking if object's "name" is preserved
      console.log('this.canvas.item(0).name');
      console.log(this.canvas);
    });

  }
  getProducts() {
    this.spinner.show();
    this.shop.getBoardItems().subscribe((res: any) => this.onSuccess(res));
  }
  onSuccess(res) {
    console.log(res);
    if (res !== undefined) {
      this.spinner.hide();
      if (res.message === undefined) {
        res.forEach((item: any) => {
          this.data.push(item);
        });
      }
    }
  }

  rasterizeJSON() {
    console.log(this.canvas);
    this.json = JSON.stringify(this.canvas, null, 2);
  }

  resetPanels() {
    this.textEditor = false;
    this.imageEditor = false;
    this.figureEditor = false;
  }


}

