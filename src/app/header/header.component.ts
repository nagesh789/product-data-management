import { Component, OnInit } from '@angular/core';
import { AuthenticateService } from '../services/authenticate.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  showMenu = false;
  searchString = '';
  constructor(public auth: AuthenticateService, private rte: Router) { }
  toggleMenu() {
    this.showMenu =  !this.showMenu;
    this.auth.changeMessage(this.showMenu);
  }
  ngOnInit() {
    this.auth.currentMessage.subscribe(message => this.showMenu = message);
  }
  logout() {
    this.auth.logout();
    this.rte.navigate(['/login']);
  }
  navigate(type){

    if (type === 'dashboard'){
      this.rte.navigate(['/dashboard']);
    } else if (type === 'items') {
      this.rte.navigate(['/items']);
    } else if (type === 'bookmarklet') {
      this.rte.navigate(['/bookmarklet']);
    } else if (type === 'moodboard') {
      this.rte.navigate(['/moodboards']);
    } else if (type === 'mymoodboards') {
      this.rte.navigate(['/mymoodboards']);
    }
  }
  search() {
    if (this.searchString !==  null) {
      this.rte.navigate(['/search'], {queryParams: { search: this.searchString}});
    }
  }
}
